#include "IEX.h"
#include "../Dbms/dbms.h"
#include <stdarg.h>
#include <assert.h>
#include <fstream>
#include <chrono>
#include <thread>


void LogMessage(const char* format, ...)
{
	va_list list;
	va_start(list, format);
	vprintf(format, list);
	printf("\n");
	fflush(stdout);
	va_end(list);
}


int main()
{
	uint32_t pollInterval = 0;
	std::vector<CompanyRecord> companies;
	{
		Json::Value config;
		std::ifstream file("input.json");
		if ( file.is_open() )
			file >> config;
		if ( config.isNull() )
		{
			LogMessage("ERROR: failed to load configuration from input.json");
			return 1;
		}
		if ( config.isMember("pollInterval") &&
			config["pollInterval"].isNumeric() )
			pollInterval = config["pollInterval"].asUInt();
		Json::ArrayIndex n = 0;
		if ( config.isMember("pollSymbols") &&
			config["pollSymbols"].isArray() &&
			(n = config["pollSymbols"].size()) > 0 &&
			config["pollSymbols"][0].isString() )
		{
			companies.resize(n);
			for ( Json::ArrayIndex i = 0; i < n; ++i )
			{
				companies[i].symbol = config["pollSymbols"][i].asString();
				companies[i].price_history.resize(1, {0,0.0}); // a placeholder for newly retrieved price
			}
		}
		if ( !(30 <= pollInterval && pollInterval <= 3600) ||
			companies.empty() )
		{
			LogMessage("ERROR: configuration given in input.json is not valid");
			return 1;
		}
		// TODO: validate symbols
	}

	auto res = curl_global_init(CURL_GLOBAL_DEFAULT);
	assert(res == CURLE_OK);
	auto db_conn = dbConnect();
	assert(db_conn);
	if ( !db_conn )
		return 2;

	// retrieve once names and logos, create correspoding nodes
	for ( auto& company : companies )
	{
		company.name = IEX::stocks::company(company.symbol)["companyName"].asString();
		company.logo = IEX::stocks::logo(company.symbol)["url"].asString();
	}
	dbSetSymbols(db_conn, companies);

	// start polling loop
	while ( 1 ) // TODO: define termination condition
	{
		for ( const auto& company : companies )
			company.price_history[0].price = IEX::stocks::price(company.symbol).asDouble();
		dbUpdatePrices(db_conn, companies);
		std::this_thread::sleep_for(std::chrono::seconds(pollInterval));
		// TODO: if pollInterval is too big it worth going offline with db connection
	}

	dbDisconnect(db_conn);
	curl_global_cleanup();
}
