#pragma once

#include <curl/curl.h>
#include <json/json.h>
#include <string>
#include <vector>

namespace IEX
{
    std::vector<std::string> getSymbolList();
    
		namespace stocks
		{
        Json::Value company(const std::string &);
        Json::Value logo(const std::string &);
        Json::Value price(const std::string &);
    }
}
