#include "IEX.h"
#include <assert.h>

#define IEX_ENDPOINT "https://api.iextrading.com/1.0"

namespace IEX
{
	//Callback function used by sendGetRequest to get the result from curl.
	std::size_t callback(const char* in, std::size_t size, std::size_t num, std::string* out) {
		const std::size_t totalBytes(size * num);
		out->append(in, totalBytes);
		return totalBytes;
	}

	//Use LIBCURL to send the GET requests
	void sendGetRequest(Json::Value &jsonData, const std::string url) {
		CURL* curl = curl_easy_init();
		assert(curl);

		auto res = curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		assert(res == CURLE_OK);
		res = curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		assert(res == CURLE_OK);
		res = curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);
		assert(res == CURLE_OK);
		res = curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		assert(res == CURLE_OK);
		long int httpCode(0); //Having this as a normal int will cause a segmentation fault for some requests being too large.
		std::unique_ptr<std::string> httpData(new std::string());
		res = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);
		assert(res == CURLE_OK);
		res = curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());
		assert(res == CURLE_OK);
		res = curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
		assert(res == CURLE_OK);
		res = curl_easy_perform(curl);
		assert(res == CURLE_OK);
		res = curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
		assert(res == CURLE_OK);
		curl_easy_cleanup(curl);

		Json::Reader jsonReader;
		auto res1 = jsonReader.parse(*httpData, jsonData); //TODO error handle
		assert(res1);
	}
}

std::vector<std::string> IEX::getSymbolList()
{
	Json::Value jsonData;
	std::string url(IEX_ENDPOINT);
	url += "/ref-data/symbols";
	IEX::sendGetRequest(jsonData, url);
	std::vector<std::string> symbolList;
	assert(jsonData.isArray()); //Crash if not an array
	if ( jsonData.isArray() && jsonData.size() ) {
		symbolList.reserve(jsonData.size());
		for ( Json::ArrayIndex i = 0; i < jsonData.size(); ++i )
			symbolList.push_back(jsonData[i]["symbol"].asString());
	}
	return symbolList;
}


/* GET /stock/{symbol}/company
{
	"symbol": "AAPL",
	"companyName": "Apple Inc.",
	"exchange": "Nasdaq Global Select",
	"industry": "Computer Hardware",
	"website": "http://www.apple.com",
	"description": "Apple Inc is an American multinational technology company. It designs, manufactures, and markets mobile communication and media devices, personal computers, and portable digital music players.",
	"CEO": "Timothy D. Cook",
	"issueType": "cs",
	"sector": "Technology",
} */
Json::Value IEX::stocks::company(const std::string &symbol) {
	Json::Value jsonData;
	std::string url(IEX_ENDPOINT);
	url += "/stock/" + symbol + "/company";
	IEX::sendGetRequest(jsonData, url);
	assert(jsonData.isObject());
	return jsonData;
}


/* GET /stock/{symbol}/logo
{
	"url": "https://storage.googleapis.com/iex/api/logos/AAPL.png"
} */
Json::Value IEX::stocks::logo(const std::string &symbol) {
	Json::Value jsonData;
	std::string url(IEX_ENDPOINT);
	url += "/stock/" + symbol + "/logo";
	IEX::sendGetRequest(jsonData, url);
	assert(jsonData.isObject());
	return jsonData;
}


/* GET /stock/{symbol}/price
 198.8784488 */
Json::Value IEX::stocks::price(const std::string &symbol) {
	Json::Value jsonData;
	std::string url(IEX_ENDPOINT);
	url += "/stock/" + symbol + "/price";
	IEX::sendGetRequest(jsonData, url);
	assert(jsonData.isNumeric());
	return jsonData;
}
