#include "../Dbms/dbms.h"
#include "cpprest/http_listener.h"
#include <stdarg.h>
#include <assert.h>


void LogMessage(const char* format, ...)
{
	va_list list;
	va_start(list, format);
	vprintf(format, list);
	printf("\n");
	fflush(stdout);
	va_end(list);
}


using namespace web;
using namespace http;
using namespace utility;
using namespace http::experimental::listener;


DB_CONNECTION* db_conn = nullptr;

inline void serve_response(const http_request& message,
	status_code rsp_code, const json::value& rsp_body)
{
	auto completion = [](pplx::task<void> t) {
		try {
			t.get();
		}
		catch ( std::exception& e ) {
			ucout << U("EXCEPTION: ") << e.what();
		}
	};
	pplx::task<void> t;
	switch (rsp_code)
	{
		case status_codes::OK:
			t = message.reply(rsp_code, rsp_body);
			break;
		case status_codes::BadRequest:
			t = message.reply(rsp_code, U("Bad Request"));
			break;
		case status_codes::InternalError:
		default:
			t = message.reply(rsp_code, U("Internal Error"));
	}
	t.then(completion);
}

void handle_GET(http_request message)
{
	assert(db_conn);

	web::json::value rsp_body = web::json::value::array();

	// http://address:port/Get/SymbolInfo?symbols=AA,ABC,DE
	auto paths = http::uri::split_path(message.request_uri().path());
	if ( paths.size() != 2 ||
		   paths[0].compare(U("Get")) != 0 ||
		   paths[1].compare(U("CompanyInfo")) != 0 )
	{
		serve_response(message, status_codes::BadRequest, rsp_body);
		return;
	}

	std::vector<CompanyRecord> companies;
	companies.reserve(16); // ???
	auto query_params = uri::split_query(message.request_uri().query());
	if ( query_params.size() == 1 &&
		   query_params.cbegin()->first.compare(U("symbols")) == 0 )
	{
		const auto& symbol_list = query_params.cbegin()->second;
		std::string symbol;
		symbol.reserve(8);
		for ( const auto c : symbol_list )
		{
			if ( c != U(',') ) {
				symbol.push_back((char)c);
				continue;
			}
			if ( symbol.empty() || symbol.length() > 8 ) {
				serve_response(message, status_codes::BadRequest, rsp_body);
				return;
			}
			companies.push_back({});
			std::swap(companies.back().symbol, symbol);
			assert(symbol.empty());
		}
		// the last symbol in the list
		if ( symbol.empty() || symbol.length() > 8 ) {
			serve_response(message, status_codes::BadRequest, rsp_body);
			return;
		}
		companies.push_back({});
		std::swap(companies.back().symbol, symbol);
		assert(symbol.empty());
	}
	// Note: Duplicate symbols are ok
	if ( companies.empty() ) {
		serve_response(message, status_codes::BadRequest, rsp_body);
		return;
	}

	if ( !dbQueryPrices(db_conn, companies) ) { // TODO: cache result for some time
		serve_response(message, status_codes::InternalError, rsp_body);
		return;
	}

	constexpr size_t buf_length = 256;
	utility::char_t buf[buf_length];
	size_t i = 0/*, l*/;
	for ( const auto& company : companies )
	{
		auto& rsp_company = rsp_body[i];

		/*l = company.symbol.length();
		assert(l < buf_length);
		mbstowcs(buf, company.symbol.c_str(), l);
		buf[l] = 0;*/
		rsp_company[U("symbol")] = web::json::value(company.symbol);

		/*l = company.name.length();
		assert(l < buf_length);
		mbstowcs(buf, company.name.c_str(), l);
		buf[l] = 0;*/
		rsp_company[U("name")] = web::json::value(company.name);

		/*l = company.logo.length();
		assert(l < buf_length);
		mbstowcs(buf, company.logo.c_str(), l);
		buf[l] = 0;*/
		rsp_company[U("logo")] = web::json::value(company.logo);

		auto& rsp_company_price_history = rsp_company[U("price_history")];
		rsp_company_price_history = web::json::value::array();
		size_t j = 0;
		for ( const auto& price_point : company.price_history )
		{
			auto& rsp_company_price_history_point = rsp_company_price_history[j];
			//wcsftime(buf, buf_length, L"%d-%m-%Y %H:%M", localtime(&price_point.timestamp));
			strftime(buf, buf_length, "%d-%m-%Y %H:%M", localtime(&price_point.timestamp));
			rsp_company_price_history_point[U("date_time")] = web::json::value(buf);
			rsp_company_price_history_point[U("price")] = price_point.price;
			++j;
		}

		++i;
	}

	serve_response(message, status_codes::OK, rsp_body);
};



#ifdef _WIN32
int wmain(int argc, wchar_t *argv[])
#else
int main(int argc, char *argv[])
#endif
{
	if ( argc != 2 ) {
		printf("Usage: HttpEndpoint address:port");
		return 0;
	}
	utility::string_t endpoint(U("http://")); // TODO: support for https
	endpoint.append(argv[1]);
	try {
		http_listener m_listener(endpoint);
		m_listener.support(methods::GET, std::bind(&handle_GET, std::placeholders::_1));
		m_listener.open().wait();
		db_conn = dbConnect();
		assert(db_conn);
		if ( !db_conn )
			return 1;
		while ( 1 ) { // TODO: provide some correct exit routine
			getchar();
		}
		m_listener.close().wait();
		dbDisconnect(db_conn);
	}
	catch ( std::exception& e ) {
		ucout << U("EXCEPTION: ") << e.what();
	}
	return 0;
}
