#pragma once

#include <string>
#include <vector>


// an extern logging routine
extern void LogMessage(const char* format, ...);

struct CompanyRecord
{
	std::string symbol;
	std::string name;
	std::string logo;
	struct PriceRecord
	{
		time_t timestamp;
		double price;
	};
	mutable std::vector<PriceRecord> price_history;
};

struct DB_CONNECTION;

DB_CONNECTION* dbConnect();

/* Set 'symbols' node:
 /symbols
	/<symbol1>
	  /name = company1_name,
	   logo = company1_logo_url
   <symbol2>
	  /name = company2_name,
	   logo = company2_logo_url
	 ...
   <symbolN>
	  /name = companyN_name,
	   logo = companyN_logo_url */
bool dbSetSymbols(DB_CONNECTION*, const std::vector<CompanyRecord>& companies);

/* Appends 'timestampJ' under each 'symbolI' node:
 /symbols
  ...
   /<symbolI>
	  /name = company1_name,
	   logo = company1_logo_url,
		 price_history
			/timestamp1 = priceI1,
			 timestamp2 = priceI2,
			 ...
			 timestampJ = priceIJ
	 ...
	Note: prices are taken from the_last values in each price_history;
	      timestamp of the last value is neglected */
bool dbUpdatePrices(DB_CONNECTION*, const std::vector<CompanyRecord>& companies);

bool dbQueryPrices(DB_CONNECTION*, /*__inout*/ std::vector<CompanyRecord>& companies);

void dbDisconnect(DB_CONNECTION*);
