#include <chrono>
#include <thread>
#include <assert.h>
#include "dbms.h"

#include "firebase/database.h"
#include "firebase/auth.h"
#include "firebase/app.h"
#include "firebase/util.h"



/* Wait for a Future to be completed. If the Future returns an error,
it will be logged. */
void WaitForCompletion(const firebase::FutureBase& future, const char* name) {
	while ( future.status() == firebase::kFutureStatusPending )
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	if ( future.status() != firebase::kFutureStatusComplete )
		LogMessage("ERROR: %s returned an invalid result", name);
	else if ( future.error() != 0 )
		LogMessage("ERROR: %s returned error %d: %s", name, future.error(),
		           future.error_message());
}


struct DB_CONNECTION {
	::firebase::database::Database*  database;
	::firebase::auth::Auth*          auth;
};
static ::firebase::App* s_app = nullptr;


DB_CONNECTION* dbConnect()
{
	if( !s_app )
	{
		s_app = ::firebase::App::Create();
		assert(s_app);
		if ( !s_app )
			return nullptr;
	}

	// Use ModuleInitializer to initialize both Auth and Database, ensuring no
	// dependencies are missing.
	::firebase::database::Database* database = nullptr;
	::firebase::auth::Auth* auth = nullptr;
	void* initialize_targets[] = { &auth, &database };

	const firebase::ModuleInitializer::InitializerFn initializers[] = {
			[](::firebase::App* app, void* data) {
				void** targets = reinterpret_cast<void**>(data);
				::firebase::InitResult result;
				*reinterpret_cast<::firebase::auth::Auth**>(targets[0]) =
						::firebase::auth::Auth::GetAuth(app, &result);
				return result;
			},
			[](::firebase::App* app, void* data) {
				void** targets = reinterpret_cast<void**>(data);
				::firebase::InitResult result;
				*reinterpret_cast<::firebase::database::Database**>(targets[1]) =
						::firebase::database::Database::GetInstance(app, &result);
				return result;
			} };

	::firebase::ModuleInitializer initializer;
	initializer.Initialize(s_app, initialize_targets, initializers,
		sizeof(initializers) / sizeof(initializers[0]));

	WaitForCompletion(initializer.InitializeLastResult(), "Initialize");

	if ( initializer.InitializeLastResult().error() != 0 ) {
		LogMessage("Failed to initialize Firebase libraries: %s",
			initializer.InitializeLastResult().error_message());
		std::this_thread::sleep_for(std::chrono::seconds(2)); // ?
		return nullptr;
	}

	database->set_persistence_enabled(true); // ?

	/* Sign in using Auth before accessing the database.
	The default Database permissions allow anonymous users access. This will
	work as long as your project's Authentication permissions allow anonymous signin. */
	{
		firebase::Future<firebase::auth::User*> sign_in_future = auth->SignInAnonymously();
		WaitForCompletion(sign_in_future, "SignInAnonymously");
		if ( sign_in_future.error() == firebase::auth::kAuthErrorNone )
			LogMessage("Auth: Signed in anonymously");
		else
			LogMessage("ERROR: Could not sign in anonymously. Error %d: %s",
				sign_in_future.error(), sign_in_future.error_message());
	}

	return new DB_CONNECTION{database, auth};
}



void dbDisconnect(DB_CONNECTION* db_conn)
{
	assert(db_conn);
  delete db_conn->database;
  db_conn->auth->SignOut();
  delete db_conn->auth;
  delete db_conn;
	//delete s_app;
}


bool dbSetSymbols(DB_CONNECTION* db_conn, const std::vector<CompanyRecord>& companies)
{
	if ( !db_conn )
		return false;

	auto root_ref = db_conn->database->GetReference("symbols");
	assert(root_ref.is_valid());
	auto f = root_ref.GetValue();
	WaitForCompletion(f, "Get 'symbols'");
	if ( f.error() != firebase::database::kErrorNone ) {
		LogMessage("ERROR: Get 'symbols' failed with error %d: %s", f.error(), f.error_message());
		return false;
	}
	bool res = true;
	std::map<std::string, std::string> symbol_details;
	for ( const auto& company : companies )
	{
		if ( f.result()->HasChild(company.symbol.c_str()) )
			continue; // do not overwrite already existing symbols
		auto ref = root_ref.Child(company.symbol.c_str()); // ref = /symbols/<symbol>
		assert(ref.is_valid());
		symbol_details["name"] = company.name;
		symbol_details["logo"] = company.logo;
		auto f1 = ref.SetValue(symbol_details);
		WaitForCompletion(f1, "Set 'name', 'logo'");
		if ( f1.error() != firebase::database::kErrorNone ) {
			LogMessage("ERROR: Set 'name', 'logo' failed with error %d: %s", f1.error(), f1.error_message());
			res = false;
		}
	}
	return res;
}


bool dbUpdatePrices(DB_CONNECTION* db_conn, const std::vector<CompanyRecord>& companies)
{
	if ( !db_conn )
		return false;

	auto root_ref = db_conn->database->GetReference("symbols");
	assert(root_ref.is_valid());
	char ts_str[32]{'\0'};
	sprintf(ts_str, "%lX", time(nullptr));
	bool res = true;
	for ( const auto& company : companies )
	{
		if ( company.price_history.empty() )
			continue;
		double price = company.price_history.back().price;
		assert(price > 0.0);
		auto ref = root_ref.Child(company.symbol.c_str())
			         .Child("price_history")
			         .Child(ts_str); // ref = /symbols/<symbol>/price_history/<timestamp>
		assert(ref.is_valid());
		auto f1 = ref.SetValue(price);
		WaitForCompletion(f1, "Set 'timestamp' = price");
		if ( f1.error() != firebase::database::kErrorNone ) {
			LogMessage("ERROR: Set 'timestamp' = price failed with error %d: %s", f1.error(), f1.error_message());
			res = false;
		}
	}
	return res;
}


bool dbQueryPrices(DB_CONNECTION* db_conn, /*__inout*/ std::vector<CompanyRecord>& companies)
{
	if ( !db_conn )
		return false;
	if ( companies.empty() )
		return true;

	auto root_ref = db_conn->database->GetReference("symbols");
	assert(root_ref.is_valid());

	// retrieve requested symbols
	std::vector< firebase::Future<firebase::database::DataSnapshot> > fs;
	fs.reserve(companies.size());
	for ( auto& company : companies )
	{
		assert(!company.symbol.empty());
		auto ref = root_ref.Child(company.symbol.c_str()); // ref = /symbols/<symbol>
		fs.push_back(ref.GetValue());
	}

	bool res = true;
	for ( size_t i = 0; i < companies.size(); ++i )
	{
		WaitForCompletion(fs[i], "Get 'symbol'");
		if ( fs[i].error() == firebase::database::kErrorNone ) {
			if ( fs[i].result()->exists() )
			{
				assert(fs[i].result()->children_count() == 3); // name, logo, price_history

				assert(fs[i].result()->HasChild("name"));
				auto child = fs[i].result()->Child("name");
				assert(child.value().is_string());
				companies[i].name.assign( child.value().string_value() );

				assert(fs[i].result()->HasChild("logo"));
				child = fs[i].result()->Child("logo");
				assert(child.value().is_string());
				companies[i].logo.assign( child.value().string_value() );

				assert(fs[i].result()->HasChild("price_history"));
				child = fs[i].result()->Child("price_history");
				assert(child.has_children());
				companies[i].price_history.clear();
				auto price_history = child.children();
				for ( const auto& ts_price : price_history )
				{
					char* p_end = nullptr;
					time_t timestamp = (time_t)strtoll(ts_price.key(), &p_end, 16);
					assert(p_end && *p_end == '\0');
					assert(timestamp > 0);
					assert(ts_price.value().is_double());
					double price = ts_price.value().double_value();
					assert(price > 0.0);
					companies[i].price_history.push_back({timestamp,price});
				}
			}
			else
				LogMessage("WARNING: dbQueryPrices : symbol '%s' does not exist", companies[i].symbol.c_str());
		}
		else {
			LogMessage("ERROR: Get 'symbol' failed with error %d: %s", fs[i].error(), fs[i].error_message());
			res = false;
		}
	}
	return res;
}
